namespace Chichulator.Tests
{
    public class CalculatorTests
    {
        [Fact]
        public void Add_ValidInputs_ShouldReturnSum()
        {
            // Arrange
            double num1 = 1;
            double num2 = 1;
            double expected = num1 + num2;
            Calculator calculator = new Calculator();
            // Act
            double actual = calculator.Add(num1, num2);
            // Assert - expected vs actual
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Divide_ValidInputs_ShouldReturnDivision()
        {
            // Arrange
            double num1 = 1;
            double num2 = 1;
            double expected = num1/num2;
            Calculator calculator = new Calculator();
            // Act
            double actual = calculator.Divide(num1, num2);
            // Assert - expected vs actual
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Divide_ZeroDenominator_ShouldThrowDivideByException()
        {
            // Arrange
            double num1 = 1;
            double num2 = 0;
            string expected = "You fool";
            Calculator calculator = new Calculator();
            // Act & Assert
            Exception exception = Assert.Throws<DivideByZeroException>(() => calculator.Divide(num1, num2));
            string actual = exception.Message;
            Assert.Equal(expected, actual);
        }
    }
}