﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chichulator
{
    public class Calculator
    {
        public double Add(double num1, double num2) { return num1 + num2; }

        public double Divide(double numerator, double denominator)
        {
            if (denominator == 0)
                throw new DivideByZeroException("You fool");
            return numerator / denominator;
        }
    }
}
